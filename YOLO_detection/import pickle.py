import pickle

# Abrir el archivo en modo de lectura binaria
with open('model.pkl', 'rb') as f:
    # Cargar los datos del archivo en una variable
    mis_datos = pickle.load(f)

# Hacer algo con los datos
print(mis_datos)